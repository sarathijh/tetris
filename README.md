# README #

A retro-style tetris using WebGL.

Features:

* Soft drop
* Hard drop
* Holding pieces
* T-Spins (single + double)

Playable Version: https://classic-tetris.herokuapp.com/

Controls:

* Navigate menu: left + right arrows
* Select menu: enter
* Move: left + right arrows
* Rotate CCW: z
* Rotate CW: x
* Soft drop: down arrow
* Hard drop: space
* Hold: shift