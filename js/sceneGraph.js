let SceneGraph = (function () {
  class Transform {
    constructor (x=0, y=0, z=0) {
      this._x = x;
      this._y = y;
      this._z = z;
      
      this._scaleX = 1;
      this._scaleY = 1;
      this._localScaleX = 1;
      this._localScaleY = 1;
      
      this._rotation = 0;
      
      this.visible = true;
      this._parent = null;
      this._children = new Set ();
      
      this._dirty = true;
      this._scalingOrRotationDirty = true;
      this._matrix = null;
      this._scalingRotationMatrix = null;
      this._parentMatrix = null;
    }
    
    get x () { return this._x; }
    get y () { return this._y; }
    get z () { return this._z; }
    
    set x (value) {
      this._x = value;
      this._dirty = true;
    }
    set y (value) {
      this._y = value;
      this._dirty = true;
    }
    set z (value) {
      this._z = value;
      this._dirty = true;
    }
    
    get scaleX () { return this._scaleX; }
    get scaleY () { return this._scaleY; }
    
    set scaleX (value) {
      this._scaleX = value;
      this._scalingOrRotationDirty = true;
      this._dirty = true;
    }
    set scaleY (value) {
      this._scaleY = value;
      this._scalingOrRotationDirty = true;
      this._dirty = true;
    }
    
    get localScaleX () { return this._localScaleX; }
    get localScaleY () { return this._localScaleY; }
    
    set localScaleX (value) {
      this._localScaleX = value;
      this._dirty = true;
    }
    set localScaleY (value) {
      this._localScaleY = value;
      this._dirty = true;
    }
    
    get rotation () { return this._rotation; }
    
    set rotation (value) {
      this._rotation = value;
      this._scalingOrRotationDirty = true;
      this._dirty = true;
    }
    
    get parent () { return this._parent; }
    
    set parent (value) {
      if (this._parent) {
        var p = this._parent;
        do {
          this._x += p.x;
          this._y += p.y;
          this._z += p.z;
          p = p.parent;
        } while (p);
        this._parent._children.delete (this);
      }
      this._parent = value;
      if (this._parent) {
        var p = this._parent;
        do {
          this._x -= p.x;
          this._y -= p.y;
          this._z -= p.z;
          p = p.parent;
        } while (p);
        this._parent._children.add (this);
        this.parentMatrix = this._parent.localMatrix;
      } else {
        this.parentMatrix = null;
      }
      this._dirty = true;
    }
    
    destroy () {
      if (this._parent) {
        this._parent._children.delete (this);
      }
    }
    
    get matrix () {
      return mat4.multiply (
        mat4.scaling (this._localScaleX, this._localScaleY, 1),
        this.localMatrix);
    }
    
    get localMatrix () {
      if (this._scalingOrRotationDirty) {
        this._scalingOrRotationDirty = false;
        this._scalingRotationMatrix = mat4.multiply (
          mat4.scaling (this._scaleX, this._scaleY, 1),
          mat4.rotationZ (this._rotation));
      }
      if (this._dirty) {
        this._dirty = false;
        this._matrix = mat4.multiply (
            this._scalingRotationMatrix,
            mat4.translation (this._x, this._y, this._z));
        if (this._parentMatrix) {
          this._matrix = mat4.multiply (
            this._matrix,
            this._parentMatrix);
        }
      }
      return this._matrix;
    }
    
    set parentMatrix (value) {
      this._parentMatrix = value;
      this._dirty = true;
    }
    
    updateMatricies () {
      let isDirty = this._dirty;
      for (let child of this._children) {
        if (isDirty) {
          child.parentMatrix = this.localMatrix;
        }
        child.updateMatricies ();
      }
    }
  }

  class GameObject extends Transform {
    constructor (texture, shader, vertexBuffer, indexBuffer, texCoordBuffer) {
      super ();
      
      this.texture = texture;
      this.shader = shader;
      
      this.color = [1, 1, 1, 1];
      
      this.glBuffers = {
        vertexBuffer: vertexBuffer,
        indexBuffer: indexBuffer,
        texCoordBuffer: texCoordBuffer,
      };
    }
  }

  class Sprite extends GameObject {
    constructor (texture, atlasEntryOrBuffer, shader, vertexBuffer, indexBuffer) {
      let givenTexCoordBuffer = (typeof atlasEntryOrBuffer !== 'string');
      
      super (
        texture,
        shader,
        vertexBuffer || Sprite.VertexBuffer,
        indexBuffer || Sprite.IndexBuffer,
        givenTexCoordBuffer ? 
          atlasEntryOrBuffer : 
          texture.atlas [atlasEntryOrBuffer].texCoordBuffer);
      
      if (!givenTexCoordBuffer && !vertexBuffer) {
        this.width = texture.atlas [atlasEntryOrBuffer].width;
        this.height = texture.atlas [atlasEntryOrBuffer].height;
      }
    }
    
    set sprite (value) {
      this.glBuffers.texCoordBuffer = this.texture.atlas [value].texCoordBuffer;
    }
    
    get width () { return this.localScaleX; }
    get height () { return this.localScaleY; }
    
    set width (value) { this.localScaleX = value; }
    set height (value) { this.localScaleY = value; }
  }

  webgl.onInit.push (function () {
    let vertices = [
      0,  0,  0,
      0,  1,  0,
      1,  1,  0,
      1,  0,  0,
    ];
    
    let indices = [
      0, 2, 1,
      2, 0, 3,
    ];
    
    let gl = webgl.gl;
    
    Sprite.VertexBuffer = gl.createBuffer ();
    gl.bindBuffer (gl.ARRAY_BUFFER, Sprite.VertexBuffer);
    gl.bufferData (gl.ARRAY_BUFFER, new Int16Array (vertices), gl.STATIC_DRAW);
    Sprite.VertexBuffer.itemSize = 3;
    Sprite.VertexBuffer.numItems = vertices.length;
    
    Sprite.IndexBuffer = gl.createBuffer ();
    gl.bindBuffer (gl.ELEMENT_ARRAY_BUFFER, Sprite.IndexBuffer);
    gl.bufferData (gl.ELEMENT_ARRAY_BUFFER, new Uint16Array (indices), gl.STATIC_DRAW);
    Sprite.IndexBuffer.itemSize = 3;
    Sprite.IndexBuffer.numItems = indices.length;
  });

  class FixedWidthText extends GameObject {
    constructor (texture, fontInfo, shader) {
      super (
        texture,
        shader,
        webgl.gl.createBuffer (),
        webgl.gl.createBuffer (),
        webgl.gl.createBuffer ());
      
      this.fontInfo = fontInfo;
      this._text = '';
    }
    
    set text (value) {
      if (this._text == value) {
        return;
      }
      this._text = value;
      
      var vertices = [];
      var indices = [];
      var uvs = [];
      
      if (this._text.length > 0) {
        var x = 0;
        var y = 0;
        
        let firstEntry = this.texture.atlas [this._text [0]];
        let w = firstEntry.width;
        let h = firstEntry.height;
        
        let maxWidth = w * this._text.length + (this._text.length - 1) * this.fontInfo.letterSpacing;
        let maxHeight = h;
        
        let sw = w;
        let sh = h;
        
        this.width = maxWidth;
        this.height = maxHeight;
        
        var junkChars = 0;
        for (var i = 0; i < this._text.length; ++i) {
          let c = this._text [i];
          
          let atlasEntry = this.texture.atlas [c];
          
          if (atlasEntry) {
            vertices.push (...[
              x+ 0,  y+ 0,  0,
              x+ 0,  y+sh,  0,
              x+sw,  y+sh,  0,
              x+sw,     0,  0,
            ]);
            
            let o = (i - junkChars) * 4;
            indices.push (...[
              o+0, o+2, o+1,
              o+2, o+0, o+3,
            ]);
            
            // TODO: This can be calculated all at once when the atlas is loaded
            // Just calculate the array of coords rather than the OpenGL buffer
            let l = atlasEntry.x / this.texture.width;
            let r = l + (atlasEntry.width / this.texture.width);
            let t = atlasEntry.y / this.texture.height;
            let b = t + (atlasEntry.height / this.texture.height);
            
            uvs.push (...[
              l, b,
              l, t,
              r, t,
              r, b,
            ]);
          } else {
            junkChars += 1;
          }
          
          x += sw + this.fontInfo.letterSpacing;
        }
      } else {
        this.width = 0;
        this.height = 0;
      }
      
      let gl = webgl.gl;
      
      gl.bindBuffer (gl.ARRAY_BUFFER, this.glBuffers.vertexBuffer);
      gl.bufferData (gl.ARRAY_BUFFER, new Int16Array (vertices), gl.STATIC_DRAW);
      this.glBuffers.vertexBuffer.itemSize = 3;
      this.glBuffers.vertexBuffer.numItems = vertices.length;
      
      gl.bindBuffer (gl.ARRAY_BUFFER, this.glBuffers.texCoordBuffer);
      gl.bufferData (gl.ARRAY_BUFFER, new Float32Array (uvs), gl.STATIC_DRAW);
      this.glBuffers.texCoordBuffer.itemSize = 2;
      this.glBuffers.texCoordBuffer.numItems = uvs.length;
      
      gl.bindBuffer (gl.ELEMENT_ARRAY_BUFFER, this.glBuffers.indexBuffer);
      gl.bufferData (gl.ELEMENT_ARRAY_BUFFER, new Uint16Array (indices), gl.STATIC_DRAW);
      this.glBuffers.indexBuffer.itemSize = 3;
      this.glBuffers.indexBuffer.numItems = indices.length;
    }
  }

  return {
    Transform,
    GameObject,
    Sprite,
    FixedWidthText,
  };
})();
