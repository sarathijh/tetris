// require scene-graph.js

class TetrisField extends SceneGraph.Transform {
  constructor (blocksWide, blocksHigh, blockWidth, blockHeight) {
    super ();
    
    // Store information about the field's dimensions
    this.blockWidth = blockWidth;
    this.blockHeight = blockHeight;
    this.blocksWide = blocksWide;
    this.blocksHigh = blocksHigh;
    
    this.buildLines ();
  }
  
  buildLines () {
    // Create a doubly-linked list of lines, each containing an array of
    // the cells at that row.
    // An extra line is created under the bottom of the field for checking
    // if a piece has hit the bottom, and an extra two lines are created above
    // the top of the field for check if a piece was placed outside the field.
    var prev = null;
    for (var i = -2; i < this.blocksHigh+1; ++i) {
      var line;
      
      if (i < 0 || i >= this.blocksHigh) {
        line = {};
      } else {
        // Create a new Transform to contain the blocks in that line
        line = new SceneGraph.Transform ();
        line.parent = this;
        line.x = 0;
        line.z = 0;
        
        line.columns = new Array (this.blocksWide);
        for (var j = 0; j < this.blocksWide; ++j) {
          line.columns [j] = 0;
        }
      }
      
      line.y = this.blockHeight * (this.blocksHigh - i - 1);
      
      if (i == 0) {
        this.first = line;
        this.top = line;
      }
      if (i == this.blocksHigh-1) {
        this.last = line;
      }
      
      line.row = i;
      
      if (prev) {
        line.prev = prev;
        prev.next = line;
      }
      
      prev = line;
    }
  }
  
  addGarbageLines (numLines) {
    var line = this.first;
    while (line != this.last.next) {
      line.y += this.blockHeight * numLines;
      line = line.next;
    }
    
    let lines = [];
    for (var i = 0; i < numLines; ++i) {
      this.top = this.top.next;
      
      let line = new SceneGraph.Transform ();
      line.parent = this;
      line.x = 0;
      line.y = this.last.y - this.blockHeight;
      line.z = 0;
      
      line.row = this.blocksHigh + i;
      line.isGarbage = true;
      
      line.prev = this.last;
      line.next = this.last.next;
      
      this.last.next.prev = line;
      this.last.next = line;
      this.last = line;
      
      line.columns = new Array (this.blocksWide);
      for (var j = 0; j < this.blocksWide; ++j) {
        line.columns [j] = 0;
      }
      
      lines.push (line);
    }
    
    return lines;
  }
  
  removeLines (linesToRemove, lowestLineToRemove) {
    var line = lowestLineToRemove;
    var base = line.next;
    
    // TODO: This could be optimized to not go through all the lines above
    // the lowest one cleared, but stop after the last one was found
    while (line.row >= 0) {
      var prev = line.prev;
      if (linesToRemove.delete (line)) {
        if (line.isGarbage) {
          // If this is a garbage line, then just remove it
          prev.next = line.next;
          line.next.prev = prev;
          
          this.top = this.top.prev;
          
          line.destroy ();
        } else {
          // Move all lines that were cleared to the top of the field
          line.row = 0;
          line.y = (this.blocksHigh - 1) * this.blockHeight;
          
          prev.next = line.next;
          line.next.prev = prev;
          
          line.prev = this.first.prev;
          line.prev.next = line;
          line.next = this.first;
          line.next.prev = line;
          
          if (this.first == this.top) {
            this.top = line;
          } else {
            this.top = this.top.prev;
          }
          this.first = line;
        }
        
        if (line == this.last) {
          this.last = prev;
        }
      } else {
        // If the line wasn't cleared, then move it to the line above the
        // current base line
        line.row = base.isGarbage ? this.blocksHigh - 1 : base.row - 1;
        line.y = base.y + this.blockHeight;
        base = line;
      }
      line = prev;
    }
  }
  
  cellOutsideField (col, line) {
    return (
      col < 0 || 
      col > this.blocksWide-1 || 
      line.row < 0 || 
      (!line.isGarbage && line.row > this.blocksHigh-1)
      );
  }
  
  cellOccupied (col, line) {
    // Allow tetrominoes to exceed the top boundary
    if (col >= 0 && col < this.blocksWide && line.row < 0) {
      return false;
    }
    return (this.cellOutsideField (col, line) || line.columns [col] != 0);
  }
}
