let SPRITE_ATLAS_INFO = {
  'title':           { x:   1, y: 193, width: 378, height:  35, },
  'arrow_left':      { x:   1, y: 175, width:  17, height:  17, },
  'arrow_right':     { x:  19, y: 175, width:  17, height:  17, },
  'solo_icon_on':    { x:   1, y: 230, width:  83, height: 143, },
  'battle_icon_on':  { x: 169, y: 230, width:  83, height: 171, },
  'solo_icon_off':   { x:  85, y: 230, width:  83, height: 143, },
  'battle_icon_off': { x: 253, y: 230, width:  83, height: 171, },
  'white':           { x:  20, y: 144, width:   1, height:   1, },
  'ghost':           { x:  33, y: 143, width:  16, height:  14, },
  'garbage':         { x:   1, y: 143, width:  16, height:  14, },
  'perfect_clear':   { x: 159, y:   1, width: 124, height:  50, },
};

for (var i = 0; i <= 9; ++i) {
  let row = 1 + i * 8;
  
  /*SPRITE_ATLAS_INFO ['block1_' + i] = { x:  1, y: row, width: 8, height: 7, noBuffer: true, };
  SPRITE_ATLAS_INFO ['block2_' + i] = { x: 10, y: row, width: 8, height: 7, noBuffer: true, };
  SPRITE_ATLAS_INFO ['block3_' + i] = { x: 19, y: row, width: 8, height: 7, noBuffer: true, };*/
  
  SPRITE_ATLAS_INFO ['' + i] = { x: 51, y: row, width: 7, height: 7, noBuffer: true, };
}

for (var i = 0; i <= 9; ++i) {
  let row = 1 + i * 14;
  
  SPRITE_ATLAS_INFO ['block1_' + i] = { x:  1, y: row, width: 16, height: 14, noBuffer: true, };
  SPRITE_ATLAS_INFO ['block2_' + i] = { x: 17, y: row, width: 16, height: 14, noBuffer: true, };
  SPRITE_ATLAS_INFO ['block3_' + i] = { x: 33, y: row, width: 16, height: 14, noBuffer: true, };
}

let chars = ['A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
             'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
             '?'];
for (var i = 0; i < chars.length; ++i) {
  let char = chars [i];
  let col = 59 + Math.floor (i / 10) * 8;
  let row = 1 + (i % 10) * 8;
  
  SPRITE_ATLAS_INFO [char] = { x: col, y: row, width: 7, height: 7, noBuffer: true, };
}
