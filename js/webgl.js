// public
var webgl = {};


// private
(function () {
  var gl;
  
  var scene = new Set ();
  var texturesToLoad = new Set ();
  var onDoneLoadingResources = null;
  
  var fullSpriteTexCoordBuffer;
  
  
  webgl.onInit = [initFullSpriteTexCoordBuffer];
  
  
  
  
  Object.defineProperty (webgl, 'bgcolor', {
    set: function (value) {
      webgl.gl.clearColor (...value);
    }
  });
  
  
  
  
  webgl.init = 
  function (canvas, width, height) {
    gl = canvas.getContext ('webgl2', { alpha: false, antialias: true, });
    
    if (!gl) {
      console.log ('Unable to initialize WebGL. Your browser may not support it.');
      return null;
    }
    
    gl.clearColor (0, 0, 0, 1);
    gl.enable (gl.BLEND);
    gl.blendFunc (gl.SRC_ALPHA, gl.ONE_MINUS_SRC_ALPHA);
    gl.enable (gl.DEPTH_TEST);
    gl.depthFunc (gl.LEQUAL);
    
    canvas.width = width;
    canvas.height = height;
    webgl.canvas = canvas;
    
    webgl.projectionMatrix = mat4.ortho (width, height);
    
    webgl.gl = gl;
    
    for (let cb of webgl.onInit) {
      cb ();
    }
  };
  
  
  
  
  // reads the source of a shader from
  function loadSingleShaderFromDOM (id) {
    var shaderScript = document.getElementById (id);
    if (! shaderScript) {
      return null;
    }

    var shader;
    if (shaderScript.type == 'x-shader/x-vertex') {
      shader = gl.createShader (gl.VERTEX_SHADER);
    } else if (shaderScript.type == 'x-shader/x-fragment') {
      shader = gl.createShader (gl.FRAGMENT_SHADER);
    } else {
      return null;
    }

    gl.shaderSource (shader, shaderScript.innerText);
    gl.compileShader (shader);

    if (! gl.getShaderParameter (shader, gl.COMPILE_STATUS)) {
      console.log (gl.getShaderInfoLog (shader));
      return null;
    }
    
    return shader;
  }
  
  
  webgl.shaderFromDOM = 
  function (vertID, fragID) {
    var vertexShader = loadSingleShaderFromDOM (vertID);
    var fragmentShader = loadSingleShaderFromDOM (fragID);

    var program = gl.createProgram ();
    gl.attachShader (program, vertexShader);
    gl.attachShader (program, fragmentShader);
    gl.linkProgram (program);

    if (!gl.getProgramParameter (program, gl.LINK_STATUS)) {
      console.log ('Could not initialize shaders');
      return null;
    }
    
    let attributes = new Set ();
    attributes.add ('vertex');
    attributes.add ('texCoord');
    
    let uniforms = new Set ();
    uniforms.add ('mvpMatrix');
    uniforms.add ('color');
    
    // Store shader attribute locations
    program.attributes = {};
    for (let name of attributes) {
      program.attributes [name] = gl.getAttribLocation (program, name);
      gl.enableVertexAttribArray (program.attributes [name]);
    }
    
    program.uniforms = {};
    for (let name of uniforms) {
      program.uniforms [name] = gl.getUniformLocation (program, name);
    }
    
    return program;
  };
  
  
  
  
  function initFullSpriteTexCoordBuffer () {
    if (!fullSpriteTexCoordBuffer) {
      let texCoordData = [
      0, 1,
      0, 0,
      1, 0,
      1, 1,
      ];
      
      fullSpriteTexCoordBuffer = gl.createBuffer ();
      gl.bindBuffer (gl.ARRAY_BUFFER, fullSpriteTexCoordBuffer);
      gl.bufferData (gl.ARRAY_BUFFER, new Float32Array (texCoordData), gl.STATIC_DRAW);
      fullSpriteTexCoordBuffer.itemSize = 2;
      fullSpriteTexCoordBuffer.numItems = texCoordData.length;
    }
  }
  
  
  webgl.updateBufferFromAtlasInfo = 
  function (texture, name, buffer) {
    let entry = texture.atlas [name];
    
    let l = entry.x / texture.width;
    let r = l + (entry.width / texture.width);
    let t = entry.y / texture.height;
    let b = t + entry.height / texture.height;
    
    let texCoordData = [
    l, b,
    l, t,
    r, t,
    r, b,
    ];
    
    gl.bindBuffer (gl.ARRAY_BUFFER, buffer);
    gl.bufferData (gl.ARRAY_BUFFER, new Float32Array (texCoordData), gl.STATIC_DRAW);
    buffer.itemSize = 2;
    buffer.numItems = texCoordData.length;
  };
  
  
  function loadSingleTexture (texInfo) {
    var image = new Image ();
    image.src = texInfo.file;
    image.addEventListener ('load', function() {
      gl.bindTexture (gl.TEXTURE_2D, texInfo.texture);
      gl.texImage2D (gl.TEXTURE_2D, 0, gl.RGBA, gl.RGBA, gl.UNSIGNED_BYTE, image);
      
      gl.pixelStorei (gl.UNPACK_PREMULTIPLY_ALPHA_WEBGL, true);
      if (texInfo.options.wrap == 'repeat') {
        gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.REPEAT);
        gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.REPEAT);
      } else {
        gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_WRAP_S, gl.CLAMP_TO_EDGE);
        gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_WRAP_T, gl.CLAMP_TO_EDGE);
      }
      gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
      gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
      
      texInfo.texture.width = this.width;
      texInfo.texture.height = this.height;
      
      if (texInfo.texture.atlas) {
        let sharedBuffers = {};
        
        for (let name of Object.keys (texInfo.texture.atlas)) {
          let entry = texInfo.texture.atlas [name];
          
          if (entry.noBuffer) {
            continue;
          }
          
          if (entry.sharedBuffer && sharedBuffers.hasOwnProperty (entry.sharedBuffer)) {
            entry.texCoordBuffer = sharedBuffers [entry.sharedBuffer];
            continue;
          }
          
          entry.texCoordBuffer = gl.createBuffer ();
          webgl.updateBufferFromAtlasInfo (texInfo.texture, name, entry.texCoordBuffer);
          
          if (entry.sharedBuffer) {
            sharedBuffers [entry.sharedBuffer] = entry.texCoordBuffer;
          }
        }
        
        texInfo.texture.sharedBuffers = sharedBuffers;
      } else {
        texInfo.texture.atlas = {
          'full': {
            texCoordBuffer: fullSpriteTexCoordBuffer,
            width: this.width,
            height: this.height,
          },
        };
      }
      
      texturesToLoad.delete (texInfo);
      isDoneLoadingResources ();
    });
  }
  
  
  webgl.loadTexture =
  function (file, options, atlas) {
    var texture = gl.createTexture ();
    
    texture.width = 0;
    texture.height = 0;
    texture.atlas = atlas;
    
    texturesToLoad.add ({
      file: file,
      texture: texture,
      options: options,
    });
    
    return texture;
  };
  
  
  webgl.colorTexture = 
  function (rgba) {
    var texture = gl.createTexture ();
    
    texture.width = 1;
    texture.height = 1;
    texture.atlas = {
      'full': {
        texCoordBuffer: fullSpriteTexCoordBuffer,
      },
    };
    
    gl.bindTexture (gl.TEXTURE_2D, texture);
    gl.texImage2D (gl.TEXTURE_2D, 0, gl.RGBA, 1, 1, 0, gl.RGBA, gl.UNSIGNED_BYTE, new Uint8Array(rgba));
    
    gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_MIN_FILTER, gl.NEAREST);
    gl.texParameteri (gl.TEXTURE_2D, gl.TEXTURE_MAG_FILTER, gl.NEAREST);
    
    return texture;
  };
  
  
  
  
  function isDoneLoadingResources () {
    if (texturesToLoad.size == 0) {
      if (onDoneLoadingResources) {
        onDoneLoadingResources ();
      }
      return true;
    }
    return false;
  }
  
  
  webgl.loadResources = 
  function (onComplete) {
    onDoneLoadingResources = onComplete;
    if (! isDoneLoadingResources ()) {
      for (let texInfo of texturesToLoad) {
        loadSingleTexture (texInfo);
      }
    }
  };
  
  
  
  
  webgl.addDrawable = 
  function (drawable) {
    scene.add (drawable);
  };
  
  webgl.removeDrawable = 
  function (drawable) {
    scene.delete (drawable);
  };
  
  
  
  
  webgl.clear = 
  function () {
    gl.viewport (0, 0, webgl.canvas.width, webgl.canvas.height);
    gl.clear (gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
  }
  
  
  
  
  webgl.drawScene = 
  function (scene) {
    for (let drawable of scene) {
      if (drawable.visible && (!drawable.parent || drawable.parent.visible)) {
        webgl.drawSprite (drawable);
      }
    }
  };
  
  
  
  
  webgl.drawSprite = 
  function (go) {
    gl.useProgram (go.shader);
    
    gl.activeTexture (gl.TEXTURE0);
    gl.bindTexture (gl.TEXTURE_2D, go.texture);
    gl.uniform1i (go.shader.uniforms.texture, 0);
    
    gl.bindBuffer (gl.ARRAY_BUFFER, go.glBuffers.vertexBuffer);
    gl.vertexAttribPointer (go.shader.attributes.vertex, go.glBuffers.vertexBuffer.itemSize, gl.SHORT, false, 0, 0);
    
    gl.bindBuffer (gl.ARRAY_BUFFER, go.glBuffers.texCoordBuffer);
    gl.vertexAttribPointer (go.shader.attributes.texCoord, go.glBuffers.texCoordBuffer.itemSize, gl.FLOAT, false, 0, 0);
    
    gl.uniformMatrix4fv (go.shader.uniforms.mvpMatrix, false, mat4.multiply (go.matrix, webgl.projectionMatrix));
    gl.uniform4fv (go.shader.uniforms.color, go.color);
    
    gl.bindBuffer (gl.ELEMENT_ARRAY_BUFFER, go.glBuffers.indexBuffer);
    gl.drawElements (gl.TRIANGLES, go.glBuffers.indexBuffer.numItems, gl.UNSIGNED_SHORT, go.glBuffers.indexBuffer);
  };
})();
