class SpriteBuffers {
  init () {
    let gl = webgl.gl;
    
    let mesh = {
      vertices: [
         0,  0,  0,
         0,  1,  0,
         1,  1,  0,
         1,  0,  0,
      ],
      indices: [
        0, 2, 1,
        2, 0, 3,
      ],
    };
    
    this.vertexBuffer = gl.createBuffer ();
    gl.bindBuffer (gl.ARRAY_BUFFER, this.vertexBuffer);
    gl.bufferData (gl.ARRAY_BUFFER, new Float32Array (mesh.vertices), gl.STATIC_DRAW);
    this.vertexBuffer.itemSize = 3;
    this.vertexBuffer.numItems = mesh.vertices.length;
    
    this.indexBuffer = gl.createBuffer ();
    gl.bindBuffer (gl.ELEMENT_ARRAY_BUFFER, this.indexBuffer);
    gl.bufferData (gl.ELEMENT_ARRAY_BUFFER, new Uint16Array (mesh.indices), gl.STATIC_DRAW);
    this.indexBuffer.itemSize = 3;
    this.indexBuffer.numItems = mesh.indices.length;
  }
}
let spriteBuffers = new SpriteBuffers ();
webgl.onInit.push (spriteBuffers.init.bind (spriteBuffers));


class Transform {
  constructor (x=0, y=0, z=0) {
    this._x = x;
    this._y = y;
    this._z = z;
    
    this._matrix = null;
    this._matrixDirty = true;
    
    this.visible = true;
    
    this.children = new Set ();
  }
  
  get x () { return this._x; }
  get y () { return this._y; }
  get z () { return this._z; }
  
  set x (value) {
    this._x = value;
    this._matrixDirty = true;
  }
  set y (value) {
    this._y = value;
    this._matrixDirty = true;
  }
  set z (value) {
    this._z = value;
    this._matrixDirty = true;
  }
  
  get matrix () {
    if (this._matrixDirty) {
      this._matrixDirty = false;
      this._matrix = mat4.translation (this._x, this._y, this._z);
    }
    return this._matrix;
  }
}


class GameObject extends Transform {
  constructor (texture, shader, vertexBuffer, indexBuffer, texCoordBuffer) {
    super ();
    
    this.color = [1, 1, 1, 1];
    
    this._width = 0;
    this._height = 0;
    
    this._parent = null;
    
    this.shader = shader;
    this.texture = texture;
    
    this.glBuffers = {
      vertexBuffer: vertexBuffer,
      indexBuffer: indexBuffer,
      texCoordBuffer: texCoordBuffer,
    };
  }
  
  destroy () {
    if (this._parent) {
      this._parent.children.delete (this);
    }
  }
  
  get width () { return this._width; }
  get height () { return this._height; }
  
  get parent () { return this._parent; }
  
  set width (value) {
    this._width = value;
    this._matrixDirty = true;
  }
  set height (value) {
    this._height = value;
    this._matrixDirty = true;
  }
  
  set parent (value) {
    if (this._parent) {
      this._x += this._parent.x;
      this._y += this._parent.y;
      this._z += this._parent.z;
      this._parent.children.delete (this);
    }
    this._parent = value;
    if (this._parent) {
      this._x -= this._parent.x;
      this._y -= this._parent.y;
      this._z -= this._parent.z;
      this._parent.children.add (this);
    }
    this._matrixDirty = true;
  }
  
  get matrix () {
    if (this._matrixDirty) {
      this._matrixDirty = false;
      this._matrix = mat4.multiply (
        mat4.scaling (this._width, this._height, 1),
        mat4.translation (this._x, this._y, this._z));
    }
    
    if (this._parent) {
      return mat4.multiply (this._matrix, this._parent.matrix);
    } else {
      return this._matrix;
    }
  }
}


class Sprite extends GameObject {
  constructor (texture, name, shader, width, height, buffer) {
    super (
      texture,
      shader,
      spriteBuffers.vertexBuffer,
      spriteBuffers.indexBuffer,
      buffer ? buffer : texture.atlas [name].texCoordBuffer);
    
    if (width) {
      this._width = width;
    } else {
      this._width = texture.atlas [name].width;
    }
    
    if (height) {
      this._height = height;
    } else {
      this._height = texture.atlas [name].height;
    }
  }
}


class FixedWidthText extends GameObject {
  constructor (texture, shader, fontInfo) {
    let gl = webgl.gl;
    
    super (
      texture,
      shader,
      gl.createBuffer (),
      gl.createBuffer (),
      gl.createBuffer ());
    
    this.fontInfo = fontInfo;
    this._text = '';
  }
  
  set text (value) {
    if (this._text == value) {
      return;
    }
    this._text = value;
    
    var vertices = [];
    var indices = [];
    var uvs = [];
    
    if (this._text.length > 0) {
      var x = 0;
      var y = 0;
      
      var firstEntry = null;
      var w = 0;
      var h = 0;
      for (var i = 0; i < this._text.length; ++i) {
        if (this.texture.atlas.hasOwnProperty (this._text [i])) {
          firstEntry = this.texture.atlas [this._text [i]];
          w = firstEntry.width;
          h = firstEntry.height;
          break;
        }
      }
      
      let maxWidth = w * this._text.length + (this._text.length - 1) * this.fontInfo.letterSpacing;
      let maxHeight = h;
      
      let sw = w / maxWidth;
      let sh = h / maxHeight;
      
      this.width = maxWidth;
      this.height = maxHeight;
      
      var junkChars = 0;
      for (var i = 0; i < this._text.length; ++i) {
        let c = this._text [i];
        
        let atlasEntry = this.texture.atlas [(this.fontInfo.prefix || '') + c];
        
        if (atlasEntry) {
          vertices.push (...[
            x+ 0,  y+ 0,  0,
            x+ 0,  y+sh,  0,
            x+sw,  y+sh,  0,
            x+sw,     0,  0,
          ]);
          
          let o = (i - junkChars) * 4;
          indices.push (...[
            o+0, o+2, o+1,
            o+2, o+0, o+3,
          ]);
          
          // TODO: This can be calculated all at once when the atlas is loaded
          // Just calculate the array of coords rather than the OpenGL buffer
          let l = atlasEntry.x / this.texture.width;
          let r = l + (atlasEntry.width / this.texture.width);
          let t = atlasEntry.y / this.texture.height;
          let b = t + (atlasEntry.height / this.texture.height);
          
          uvs.push (...[
            l, b,
            l, t,
            r, t,
            r, b,
          ]);
        } else {
          junkChars += 1;
        }
        
        x += sw + this.fontInfo.letterSpacing / maxWidth;
      }
    } else {
      this.width = 0;
      this.height = 0;
    }
    
    let gl = webgl.gl;
    
    gl.bindBuffer (gl.ARRAY_BUFFER, this.glBuffers.vertexBuffer);
    gl.bufferData (gl.ARRAY_BUFFER, new Float32Array (vertices), gl.STATIC_DRAW);
    this.glBuffers.vertexBuffer.itemSize = 3;
    this.glBuffers.vertexBuffer.numItems = vertices.length;
    
    gl.bindBuffer (gl.ARRAY_BUFFER, this.glBuffers.texCoordBuffer);
    gl.bufferData (gl.ARRAY_BUFFER, new Float32Array (uvs), gl.STATIC_DRAW);
    this.glBuffers.texCoordBuffer.itemSize = 2;
    this.glBuffers.texCoordBuffer.numItems = uvs.length;
    
    gl.bindBuffer (gl.ELEMENT_ARRAY_BUFFER, this.glBuffers.indexBuffer);
    gl.bufferData (gl.ELEMENT_ARRAY_BUFFER, new Uint16Array (indices), gl.STATIC_DRAW);
    this.glBuffers.indexBuffer.itemSize = 3;
    this.glBuffers.indexBuffer.numItems = indices.length;
  }
}
