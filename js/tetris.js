(function () {
  let TETROMINOES = ['I', 'O', 'T', 'J', 'S', 'L', 'Z'];
  
  let BLOCK_WIDTH = 16;
  let BLOCK_HEIGHT = 14;
  let BLOCK_DISPLAY_WIDTH = 16;
  let BLOCK_DISPLAY_HEIGHT = 14;
  
  let FIELD_BLOCKS_WIDE = 10;
  let FIELD_BLOCKS_HIGH = 20;
  
  let FIELD_WIDTH = FIELD_BLOCKS_WIDE * BLOCK_WIDTH;
  let FIELD_HEIGHT = FIELD_BLOCKS_HIGH * BLOCK_HEIGHT;
  let FIELD_BORDER = 2;
  
  let SIDE_WIDTH = 112;
  let INFO_WIDTH = 172;
  
  let NEXT_BOX_WIDTH = 80;
  let NEXT_BOX_HEIGHT = 48;
  
  let PADDING_TOP = 64;
  let PADDING_BOTTOM = 30;
  let TWO_PLAYER_MID_WIDTH = 65;
  
  let SOFT_DROP_DELAY = 0.0167;
  let SIDE_DELAY = 0.1;
  let LOCK_DELAY = 0.5;
  let DAS_DELAY = 0.2;
  
  let GAME_OVER_ANIM_DELAY = 0.2;
  let GAME_OVER_ANIM_LINE_DELAY = 0.05;
  let LINE_BLOCK_REMOVAL_DELAY = 0.02;
  
  let KEY_START = 13; // Enter
  let KEYS_LEFT = [37, 65, 37]; // Left, 
  let KEYS_RIGHT = [39, 68, 39]; // Right, 
  let KEYS_SOFT_DROP = [40, 83, 40]; // Down, 
  let KEYS_HARD_DROP = [32, 72, 77]; // Space, 
  let KEYS_HOLD = [16, 17, 16]; // Shift, 
  let KEYS_ROTATE_CCW = [90, 70, 188]; // Z, 
  let KEYS_ROTATE_CW = [88, 71, 190]; // X, 
  
  let PI_2 = Math.PI / 2;
  
  
  let audio = {};
  for (let tag of document.getElementsByTagName ('audio')) {
    parts = tag.id.split ('-');
    if (parts [0] == 'audio') {
      if (! audio.hasOwnProperty (parts [1])) {
        audio [parts [1]] = {};
      }
      audio [parts [1]] [parts [2]] = tag;
    }
  }
  
  let worldWidth = 2*(FIELD_WIDTH + 2*FIELD_BORDER + SIDE_WIDTH) + TWO_PLAYER_MID_WIDTH;
  let worldHeight = FIELD_HEIGHT + 2*FIELD_BORDER + PADDING_TOP + PADDING_BOTTOM;
  
  let canvas = document.getElementById ('tetris-canvas');
  webgl.init (canvas, worldWidth, worldHeight, [1, 1, 1, 1]);
  
  function updateMaxDims () {
    canvas.style.maxWidth = (canvas.width * Math.floor (window.innerWidth / canvas.width)) + 'px';
    canvas.style.maxHeight = (canvas.height * Math.floor (window.innerHeight / canvas.height)) + 'px';
  }
  updateMaxDims ();
  window.addEventListener ('resize', updateMaxDims);
  
  let textureAtlas = webgl.loadTexture ('textures/atlas.png', {}, SPRITE_ATLAS_INFO);
  let gridTexture = webgl.loadTexture ('textures/grid.png', {
    'wrap': 'repeat',
  });
  
  let fontInfo = {
    letterSpacing: 1,
  };
  
  let baseColor = [34/255, 49/255, 92/255, 1];
  
  let shader = webgl.shaderFromDOM ('shader-vert', 'shader-frag');
  
  let pauseScene = new Set ();
  
  class TetrisGame {
    constructor (blocksWide, blocksHigh, blockWidth, blockHeight, randomSeed, twoPlayer, rightSide, keys) {
      this.keys = keys;
      this.drawables = new Set ();
      
      this.rand = new MersenneTwister (randomSeed);
      
      this.specialMoveLabels = new Set ();
      this.topSpecialMoveLabel = null;
      
      this.blockTexCoordBuffers = [
        webgl.gl.createBuffer (),
        webgl.gl.createBuffer (),
        webgl.gl.createBuffer (),
      ];
      
      this.nextTetrominos = [];
      this.nextTetromino = null;
      this.holdTetromino = null;
      this.activeTetromino = null;
      
      this.score = 0;
      this.lines = 0;
      this.level = 0;
      
      this.isSoftDropping = false;
      this.isWin = false;
      this.isGameOver = false;
      this.lastMoveWasRotate = false;
      
      this.linesToRemove = null;
      this.lowestLineToRemove = null;
      
      this.isAnimatingLineRemoval = false;
      this.line_t = 0;
      this.blocks_removed = 0;
      
      this.t_drop = 0;
      this.t_das = 0;
      this.das_wait = false;
      this.t_lock_delay = 0;
      this.side_t = 0;
      this.t_game_over_drop = 0;
      this.rows_dropping = 0;
      this.sideKey = 0;
      this.isPlaceDelayed = false;
      this.dropDelay = 0;
      
      this.backToBack = false;
      this.combo = 0;
      
      this.alreadyHeld = false;
      
      this.createUI (twoPlayer, rightSide);
      
      this.field = new TetrisField (blocksWide, blocksHigh, blockWidth, blockHeight);
      this.field.parent = this.uiStatic.uiField;
      this.field.x = 0;
      this.field.y = 0;
      this.field.z = 0;
      
      this.ghostPiece = this.makeTetromino ('T', SPRITE_ATLAS_INFO.ghost.texCoordBuffer);
      this.ghostPiece.parent = this.uiStatic.uiField;
      this.ghostPiece.z = 0;
      this.ghostPiece.visible = false;
      
      this.ongameover = null;
      this.onlineclear = null;
      
      this.hardDropEffects = [];
    }
    
    addGarbageLines (numLines) {
      let lines = this.field.addGarbageLines (numLines);
      for (let line of lines) {
        let open = Math.floor (Math.random () * FIELD_BLOCKS_WIDE);
        for (var i = 0; i < FIELD_BLOCKS_WIDE; ++i) {
          if (i == open) {
            continue;
          }
          let block = new SceneGraph.Sprite (textureAtlas, 'garbage', shader);
          block.parent = line;
          block.x = BLOCK_WIDTH * i;
          block.y = 0;
          block.z = 0;
          this.drawables.add (block);
          line.columns [i] = block;
        }
      }
      this.updateGhostPieceHeight ();
      
      for (var i = 0; i < numLines; ++i) {
        this.activeTetromino.line = this.activeTetromino.line.next;
      }
      
      if (this.activeTetromino.y < this.ghostPiece.y) {
        this.activeTetromino.y = this.ghostPiece.y;
        this.activeTetromino.line = this.ghostPiece.line;
      }
    }
    
    startGame () {
      this.updateTexCoordBuffers ();
      this.dropDelay = dropDelayForLevel (this.level);
      
      this.updateNextTeromino ();
      this.dropNextTetromino ();
    }
    
    makeTetromino (kind, buffer) {
      let tetromino = new Tetromino (kind, this.field, buffer);
      for (let block of tetromino.blocks) {
        this.drawables.add (block);
      }
      return tetromino;
    }
    
    updateMove () {
      if (this.side_t > 0) {
        this.side_t -= game.deltaTime;
      }
      if (this.side_t < 0) {
        this.side_t = 0;
      }
      if (this.t_das > 0) {
        this.t_das -= game.deltaTime;
      }
      if (this.das_wait && this.t_das <= 0) {
        this.das_wait = false;
        this.side_t = 0;
      }
      
      if (game.keyPressedThisFrame [this.keys.left]) {
        this.sideKey = this.keys.left;
        this.side_t = -1;
        this.t_das = DAS_DELAY;
        this.das_wait = true;
      } else if (game.keyPressedThisFrame [this.keys.right]) {
        this.sideKey = this.keys.right;
        this.side_t = -1;
        this.t_das = DAS_DELAY;
        this.das_wait = true;
      }
      
      if (this.sideKey != 0 && game.keyDown [this.sideKey]) {
        if ((this.side_t == 0 && this.t_das <= 0) || this.side_t < 0) {
          this.side_t = SIDE_DELAY;
          
          var delta = (this.sideKey == this.keys.left ? -1 : 1);
          if (!this.activeTetromino.anyBlock ((c, l) => this.field.cellOccupied (c + delta, l))) {
            this.activeTetromino.x += BLOCK_WIDTH * delta;
            this.activeTetromino.col += delta;
            
            this.ghostPiece.x == this.activeTetromino.x;
            this.updateGhostPieceHeight ();
            
            this.lastMoveWasRotate = false;
            
            audio.sfx.move_piece.currentTime = 0;
            audio.sfx.move_piece.play ();
          }
        }
      } else {
        this.sideKey = 0;
      }
    }
    
    easeOutCubic (t) { return (--t)*t*t+1; }
    
    createHardDropEffect (dist) {
      var blocks = this.activeTetromino.info.rotations [this.activeTetromino.currentRotation];
      var cols = {};
      for (let block of blocks) {
        if (!cols.hasOwnProperty (block [0])) {
          cols [block [0]] = [block [1], block [1]];
        } else {
          if (block [1] > cols [block [0]][0]) {
            cols [block [0]][0] = block [1];
          }
          if (block [1] < cols [block [0]][1]) {
            cols [block [0]][1] = block [1];
          }
        }
      }
      console.log (cols);
      let keys = Object.keys (cols).sort ((a, b) => a - b);
      var lines = [];
      for (let x of keys) {
        for (var i = 0; i < BLOCK_WIDTH*0.5; ++i) {
          let line = new SceneGraph.Sprite (textureAtlas, 'white', shader);
          line.parent = this.uiStatic.uiField;
          line.x = this.activeTetromino.x + x*BLOCK_WIDTH + Math.floor (Math.random () * BLOCK_WIDTH);
          line.y = this.activeTetromino.y - (cols[x][0])*BLOCK_HEIGHT;
          line.z = -20;
          line.width = 1;
          line.height = Math.floor ((cols[x][0] - cols[x][1] + 1)*BLOCK_HEIGHT + Math.random () * 4 * BLOCK_HEIGHT);
          line.origHeight = line.height;
          this.drawables.add (line);
          lines.push (line);
        }
      }
      lines.t = game.time;
      lines.width = lines.length;
      lines.dist = dist;
      this.hardDropEffects.push (lines);
    }
    
    updateDrop () {
      // Sonic drop
      // Only comment out the line check if the hard drop locks the piece immediately
      if (game.keyPressedThisFrame [this.keys.hardDrop] /*&& this.activeTetromino.line != this.ghostPiece.line*/) {
        let dist = this.ghostPiece.line.row - this.activeTetromino.line.row;
        this.score += 2 * dist;
        this.updateScoreText ();
        
        this.activeTetromino.line = this.ghostPiece.line;
        this.activeTetromino.y = this.ghostPiece.y;
        this.t_drop = 0;
        this.lastMoveWasRotate = false;
        
        audio.sfx.clack.currentTime = 0;
        audio.sfx.clack.play ();
        
        this.createHardDropEffect (dist);
        
        this.placeActiveTetromino (false);
        return true;
      }
      
      if (game.keyPressedThisFrame [this.keys.softDrop]) {
        // If the player pressed the soft drop key this frame then start soft dropping
        this.isSoftDropping = true;
      } else if (!game.keyDown [this.keys.softDrop]) {
        // If the player is no longer holding the soft drop key, then stop soft dropping
        this.isSoftDropping = false;
      }
      
      var delay = this.isSoftDropping ? SOFT_DROP_DELAY : this.dropDelay;
      if (this.t_drop >= delay) {
        // If any of the blocks have an occupied space underneath them, then we
        // need to start the lock delay timer
        if (this.activeTetromino.anyBlock ((c, l) => this.field.cellOccupied (c, l.next))) {
          // If the lock delay is over, then place the active tetromino
          if (this.t_drop >= LOCK_DELAY || game.keyPressedThisFrame [this.keys.softDrop]) {
            this.t_drop = 0;
            this.placeActiveTetromino ();
            return true;
          }
        } else {
          // The drop delay is over and there's nothing underneath the piece,
          // so move it down one line
          this.t_drop = 0;
          this.lastMoveWasRotate = false;
          this.activeTetromino.line = this.activeTetromino.line.next;
          this.activeTetromino.y = this.activeTetromino.line.y;
          
          if (this.isSoftDropping) {
            this.score += 1;
            this.updateScoreText ();
          }
        }
      }
      this.t_drop += game.deltaTime;
      return false;
    }
    
    update () {
      let hardDropEffectDuration = 0.1;
      
      var i = this.hardDropEffects.length;
      while (i--) {
        let effect = this.hardDropEffects [i];
        let delta = game.time - effect.t;
        if (delta > hardDropEffectDuration) {
          for (let l of effect) {
            l.destroy ();
            this.drawables.delete (l);
          }
          this.hardDropEffects.splice (i, 1);
        } else {
          let t = delta / hardDropEffectDuration;
          for (let l of effect) {
            l.height = l.origHeight + this.easeOutCubic (t) * BLOCK_HEIGHT * 4;
            l.color [3] = 1 - this.easeOutCubic (t);
          }
        }
        /*let linesRemoved = delta * 10 * effect.width;
        let linesToRemove = linesRemoved - (effect.width - effect.length);
        if (effect.length <= linesToRemove || effect.length == linesToRemove == 1) {
          for (let l of effect) {
            l.destroy ();
            this.drawables.delete (l);
          }
          this.hardDropEffects.splice (i, 1);
        } else {
          for (var j = 0; j < linesToRemove; ++j) {
            if (effect.length < 2) {
              break;
            }
            let reml = effect.splice (0, 1);
            reml [0].destroy ();
            this.drawables.delete (reml [0]);
            
            let remr = effect.splice (-1, 1);
            remr [0].destroy ();
            this.drawables.delete (remr [0]);
          }
          for (let l of effect) {
            l.height += 20;
          }
        }*/
      }
      
      var doneAnimating = null;
      for (let label of this.specialMoveLabels) {
        let t = (game.time - label.showTime) * 5;
        if (!label.doneAnimating) {
          let d = Math.min (t, 1);
          if (d >= 1) {
            label.doneAnimating = true;
          }
          let numLetters = Math.floor (d * label.label.length);
          label.text = label.label.substring (0, numLetters);
        }
        if (t >= 5) {
          this.drawables.delete (label);
          if (!doneAnimating) {
            doneAnimating = new Set ([label]);
          } else {
            doneAnimating.add (label);
          }
        }
      }
      if (doneAnimating) {
        for (let label of doneAnimating) {
          this.specialMoveLabels.delete (label);
          if (label == this.topSpecialMoveLabel) {
            this.topSpecialMoveLabel = null;
          }
        }
      }
      
      if (this.isWin) {
        return;
      }
      
      if (this.isGameOver) {
        if (this.field.first.y > -BLOCK_HEIGHT*2) {
          if (this.t_game_over_drop >= GAME_OVER_ANIM_DELAY + GAME_OVER_ANIM_LINE_DELAY) {
            if (this.rows_dropping < FIELD_BLOCKS_HIGH) {
              this.t_game_over_drop = GAME_OVER_ANIM_DELAY;
              ++this.rows_dropping;
            }
          } else {
            this.t_game_over_drop += game.deltaTime;
          }
          var line = this.field.last;
          for (var i = 0; i < this.rows_dropping; ++i) {
            for (var block of line._children) {
              block.velocity -= 8 + Math.floor (Math.random () * 56);
              block.actualY += block.velocity * game.deltaTime;
              block.y = Math.round (block.actualY);
            }
            line = line.prev;
          }
        }
        return;
      }
      
      if (this.isAnimatingLineRemoval) {
        this.updateLineRemovalAnimation ();
        return;
      }
      
      if (game.keyPressedThisFrame [this.keys.hold]) {
        if (this.holdActiveTetromino ()) {
          return;
        }
      }
      
      var rotateDir = 0;
      if (game.keyPressedThisFrame [this.keys.rotateCCW]) {
        rotateDir = -1;
      } else if (game.keyPressedThisFrame [this.keys.rotateCW]) {
        rotateDir = +1;
      }
      if (rotateDir != 0) {
        if (this.activeTetromino.rotate (rotateDir)) {
          this.ghostPiece.currentRotation = this.activeTetromino.currentRotation;
          this.ghostPiece.updateBlockPositions ();
          this.updateGhostPieceHeight ();
          
          this.lastMoveWasRotate = true;
        }
        
        audio.sfx.rotate_piece.currentTime = 0;
        audio.sfx.rotate_piece.play ();
      }
      
      // Move the tetromino left or right
      this.updateMove ();
      
      // Move the current tetromino down one space
      // If it hits something, then it was placed, so stop updating it
      if (this.updateDrop ()) {
        return;
      }
    }
    
    draw () {
      this.uiRoot.updateMatricies ();
      this.uiStatic.draw ();
      webgl.drawScene (this.drawables);
    }
    
    
    
    updateScoreText () {
      this.uiStatic.updateScoreText (this.score);
    }
    
    updateLinesText () {
      this.uiStatic.updateLinesText (this.lines);
    }
    
    updateLevelText () {
      this.uiStatic.updateLevelText (this.level);
    }
    
    addSpecialMoveLabel (...lines) {
      var isFirst = true;
      for (let text of lines.reverse ()) {
        let label = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
        label.parent = this.uiStatic.uiSidebar;
        label.label = text;
        label.text = text;
        label.showTime = game.time;
        label.color = [0, 0, 0, 1];
        label.x = Math.floor (SIDE_WIDTH/2 - label.width/2);
        label.y = (this.topSpecialMoveLabel ? this.topSpecialMoveLabel.y + this.topSpecialMoveLabel.height + (isFirst ? 15 : 9) : 10);
        label.z = 0;
        label.text = '';
        this.drawables.add (label);
        this.specialMoveLabels.add (label);
        this.topSpecialMoveLabel = label;
        
        isFirst = false;
      }
    }
    
    createUI (twoPlayer, rightSide) {
      this.uiRoot = new SceneGraph.Transform ();
      
      this.uiStatic = new StaticUI (twoPlayer, rightSide);
      this.uiStatic.parent = this.uiRoot;
      this.uiStatic.x = 0;
      this.uiStatic.y = 0;
      this.uiStatic.z = 100;
      
      this.uiRoot.x = twoPlayer ? (rightSide ? SIDE_WIDTH + FIELD_WIDTH + TWO_PLAYER_MID_WIDTH : 0) : Math.floor (worldWidth/2 - (this.uiStatic.uiLinesText.x + this.uiStatic.uiLinesText.width*2 + 16)/2);
    }
    
    updateTexCoordBuffers () {
      // TODO: This needs to choose the highest level between the two players
      audio.music.tetris.playbackRate = 1 + this.level * 2 / 30;
      
      webgl.updateBufferFromAtlasInfo (textureAtlas, 'block1_' + (this.level % 10), this.blockTexCoordBuffers [0]);
      webgl.updateBufferFromAtlasInfo (textureAtlas, 'block2_' + (this.level % 10), this.blockTexCoordBuffers [1]);
      webgl.updateBufferFromAtlasInfo (textureAtlas, 'block3_' + (this.level % 10), this.blockTexCoordBuffers [2]);
    }
    
    shuffle (array) {
      var rand, index = -1,
        length = array.length,
        result = Array(length);
      while (++index < length) {
        rand = Math.floor(this.rand.random () * (index + 1));
        result[index] = result[rand];
        result[rand] = array[index];
      }
      return result;
    }
    
    updateNextTeromino () {
      if (this.nextTetrominos.length == 0) {
        this.nextTetrominos = this.shuffle (TETROMINOES);
      }
      let kind = this.nextTetrominos.shift ();
      let buffer = this.blockTexCoordBuffers [TetrominoInfo [kind].blockType-1];
      this.nextTetromino = this.makeTetromino (kind, buffer);
      this.nextTetromino.parent = this.uiStatic.uiNextContainer;
      this.nextTetromino.x = Math.floor (this.uiStatic.uiNextContainer.width/2 - this.nextTetromino.info.center.x * BLOCK_WIDTH);
      this.nextTetromino.y = Math.ceil (this.uiStatic.uiNextContainer.height/2 - this.nextTetromino.info.center.y * BLOCK_HEIGHT);
      this.nextTetromino.z = 0;
    }
    
    dropTetromino (tetromino) {
      this.activeTetromino = tetromino;
      
      this.activeTetromino.line = this.field.top;
      this.activeTetromino.col = Math.floor (FIELD_BLOCKS_WIDE/2);
      
      this.activeTetromino.parent = this.uiStatic.uiField;
      this.activeTetromino.x = this.activeTetromino.col * BLOCK_WIDTH;
      this.activeTetromino.y = this.activeTetromino.line.y;
      this.activeTetromino.z = -10;
      
      this.ghostPiece.visible = true;
      this.ghostPiece.currentRotation = this.activeTetromino.currentRotation;
      this.ghostPiece.kind = this.activeTetromino.kind;
      this.ghostPiece.info = this.activeTetromino.info;
      this.ghostPiece.updateBlockPositions ();
      this.updateGhostPieceHeight ();
      
      this.isSoftDropping = false;
      this.lastMoveWasRotate = false;
    }
    
    dropNextTetromino () {
      this.dropTetromino (this.nextTetromino);
      this.updateNextTeromino ();
    }
    
    updateGhostPieceHeight () {
      this.ghostPiece.col = this.activeTetromino.col;
      this.ghostPiece.x = this.activeTetromino.x;
      this.ghostPiece.line = this.activeTetromino.line;
      var prev = this.ghostPiece.line;
      while (true) {
        if (this.ghostPiece.anyBlock (this.field.cellOccupied.bind (this.field))) {
          this.ghostPiece.line = prev;
          break;
        } else {
          prev = this.ghostPiece.line;
          this.ghostPiece.line = this.ghostPiece.line.next;
        }
      }
      this.ghostPiece.y = this.ghostPiece.line.y;
    }
    
    didTSpin () {
      // A t-spin occurs if the piece cannot move left, right, or up
      if (this.activeTetromino.kind == 'T' && this.lastMoveWasRotate) {
        return (this.activeTetromino.anyBlock ((c, l) => this.field.cellOccupied (c+1, l)) &&
                this.activeTetromino.anyBlock ((c, l) => this.field.cellOccupied (c-1, l)) &&
                this.activeTetromino.anyBlock ((c, l) => this.field.cellOccupied (c, l.prev)));
      }
      return false;
    }
    
    onWin () {
      this.isWin = true;
    }
    
    doGameOver () {
      this.isGameOver = true;
      
      if (this.ongameover) {
        this.ongameover (this);
      }
      
      var line = this.field.first;
      while (line != this.field.last.next) {
        for (var block of line._children) {
          block.velocity = 0;
          block.actualY = block.y;
        }
        line = line.next;
      }
    }
    
    placeActiveTetromino (playSound=true) {
      var changedLines = new Set ();
      
      // This needs to happen before we put the new blocks in the field
      let tspin = this.didTSpin ();
      
      this.activeTetromino.forEachBlock ((col, line, block) => {
        // Game over conditions when trying to place a piece:
        //  A block is placed outside the play field, or
        //  A block is placed in an already occupied cell
        if (line.row < 0 || this.field.cellOccupied (col, line)) {
          this.isGameOver = true;
        }
        
        if (line.row >= 0) {
          // Place each block of the tetromino in its corresponding line and
          // column of the play field
          line.columns [col] = block;
          // Change the block's parent to the line it was placed in 
          block.parent = line;
          // Keep track of the lines that had blocks placed in them
          changedLines.add (line);
        }
      });
      
      this.ghostPiece.visible = false;
      
      if (this.isGameOver) {
        this.doGameOver ();
        return;
      }
      
      // Move the depth of the placed piece back, this allows the active piece
      // to be drawn on top of ones that have already been placed.
      // (This may not be necessary because the active one should always be at
      //  the end of the draw list, meaning they would be drawn on top anyway)
      //this.activeTetromino.z = 10;
      this.alreadyHeld = false;
      
      // Of the lines that were changed, determine which ones are complete and
      // mark them for removal.
      // Keep track of the lowest line to be used by the line removal algorithm
      this.linesToRemove = new Set ();
      var lowestRow = -1;
      for (var line of changedLines) {
        line.empty = false;
        var full = true;
        for (var i = 0; i < FIELD_BLOCKS_WIDE; ++i) {
          if (line.columns [i] == 0) {
            full = false;
            break;
          }
        }
        if (full) {
          this.linesToRemove.add (line);
          if (line.row > lowestRow) {
            lowestRow = line.row;
            this.lowestLineToRemove = line;
          }
        }
      }
      
      let numLines = this.linesToRemove.size;
      
      if (playSound) {
        audio.sfx.bump_piece.currentTime = 0;
        audio.sfx.bump_piece.play ();
      }
      
      var gotSpecial = false;
      var scoreBonus = 0;
      
      if (numLines > 0) {
        scoreBonus = scoreForLinesCleared (this.level, numLines);
        
        this.addSpecialMoveLabel (
          numLines == 1 ? 'SINGLE' : 
          numLines == 2 ? 'DOUBLE' : 
          numLines == 3 ? 'TRIPLE' : 
          numLines == 4 ? 'TETRIS' : 
          'WTF');
        
        gotSpecial = (numLines == 4);
      }
      
      // Update score from t-spin
      if (tspin) {
        // This overwrites the score bonus from the normal line clear
        scoreBonus = tSpinBonusForLines (this.level, numLines);
        
        this.addSpecialMoveLabel ('T SPIN');
        
        audio.sfx.tspin.currentTime = 0;
        audio.sfx.tspin.play ();
        
        gotSpecial = (numLines > 0);
      }
      
      var backToBackBonus = this.backToBack && gotSpecial;
      if (backToBackBonus) {
        this.addSpecialMoveLabel ('BACK TO', 'BACK');
        scoreBonus *= 1.5;
      }
      
      if (numLines > 0) {
        if (this.combo > 0) {
          scoreBonus += this.combo * 50;
          this.addSpecialMoveLabel (this.combo+'', 'COMBO');
        }
        this.combo += 1;
      } else {
        this.combo = 0;
      }
      
      if (scoreBonus > 0) {
        this.score += scoreBonus;
        this.updateScoreText ();
      }
      
      if (numLines > 0 || tspin) {
        this.backToBack = gotSpecial;
      }
      
      if (numLines > 0) {
        // If we completed any lines, then start the line removal animation
        this.isAnimatingLineRemoval = true;
        this.line_t = LINE_BLOCK_REMOVAL_DELAY;
        this.blocks_removed = 0;
        
        // If we completed 4 lines, then play the tetris sound effect.
        // Otherwise, play the generic line clear sound effect
        if (numLines == 4) {
          audio.sfx.tetris.currentTime = 0;
          audio.sfx.tetris.play ();
        } else {
          audio.sfx.line_clear.currentTime = 0;
          audio.sfx.line_clear.play ();
        }
      } else {
        // If we didn't clear any lines, then drop the next piece immediately
        this.dropNextTetromino ();
      }
    }
    
    holdActiveTetromino () {
      if (this.alreadyHeld) {
        return false;
      }
      
      let currentHold = this.holdTetromino;
      
      this.holdTetromino = this.activeTetromino;
      this.holdTetromino.parent = this.uiStatic.uiHoldContainer;
      this.holdTetromino.x = Math.floor (this.uiStatic.uiHoldContainer.width/2 - this.holdTetromino.info.center.x * BLOCK_WIDTH);
      this.holdTetromino.y = Math.ceil (this.uiStatic.uiHoldContainer.height/2 - this.holdTetromino.info.center.y * BLOCK_HEIGHT);
      this.holdTetromino.z = 0;
      this.holdTetromino.currentRotation = 0;
      this.holdTetromino.updateBlockPositions ();
      
      if (currentHold) {
        this.dropTetromino (currentHold);
      } else {
        this.dropNextTetromino ();
      }
      
      audio.sfx.hold.currentTime = 0;
      audio.sfx.hold.play ();
      
      this.alreadyHeld = true;
      return true;
    }
    
    updateLineRemovalAnimation () {
      if (this.line_t >= LINE_BLOCK_REMOVAL_DELAY) {
        if (this.blocks_removed == FIELD_BLOCKS_WIDE/2) {
          this.isAnimatingLineRemoval = false;
          
          let numLines = this.linesToRemove.size;
          
          this.field.removeLines (this.linesToRemove, this.lowestLineToRemove);
          
          // Update the number of lines the player has cleared
          this.lines += numLines;
          // TODO: Create setter that does this automatically?
          this.updateLinesText ();
          
          // Check if we cleared enough lines to level up
          let newLevel = levelForLines (this.lines);
          if (this.level != newLevel) {
            this.level = newLevel;
            
            audio.sfx.level_up.currentTime = 0;
            audio.sfx.level_up.play ();
            
            this.updateTexCoordBuffers ();
            this.dropDelay = dropDelayForLevel (this.level);
            this.updateLevelText ();
          }
          
          // TODO: Make this more efficient (keep track of whether a line is empty)
          var perfectClear = true;
          line = this.field.first;
          while (line.row < FIELD_BLOCKS_HIGH && perfectClear) {
            for (let c of line.columns) {
              if (c != 0) {
                perfectClear = false;
                break;
              }
            }
            line = line.next;
          }
          
          if (perfectClear) {
            audio.sfx.perfect_clear.currentTime = 0;
            audio.sfx.perfect_clear.play ();
            
            this.score += scoreForPerfectClear (this.level);
            this.updateScoreText ();
            this.addSpecialMoveLabel ('PERFECT', 'CLEAR');
          }
          
          if (this.onlineclear && numLines > 0) {
            this.onlineclear (this, numLines);
          }
          
          this.dropNextTetromino ();
        } else {
          this.line_t = 0;
          var l = FIELD_BLOCKS_WIDE/2 - this.blocks_removed - 1;
          var r = FIELD_BLOCKS_WIDE/2 + this.blocks_removed;
          // We should have already checked that every cell in the line was full
          for (var line of this.linesToRemove) {
            line.columns [l].destroy ();
            line.columns [r].destroy ();
            this.drawables.delete (line.columns [l]);
            this.drawables.delete (line.columns [r]);
            line.columns [l] = 0;
            line.columns [r] = 0;
          }
          ++this.blocks_removed;
        }
      } else {
        this.line_t += game.deltaTime;
      }
    }
  }
  
  class StaticUI extends SceneGraph.Transform {
    constructor (twoPlayer, rightSide) {
      super ();
      
      this.drawables = [];
      
      this.uiSidebar = new SceneGraph.Transform ();
      this.uiSidebar.parent = this;
      this.uiSidebar.y = PADDING_BOTTOM;
      this.uiSidebar.z = 0;
      
      this.uiFieldBorder = new SceneGraph.Sprite (textureAtlas, 'white', shader);
      this.uiFieldBorder.parent = this;
      this.uiFieldBorder.color = baseColor;
      this.uiFieldBorder.width = FIELD_WIDTH + 2*FIELD_BORDER;
      this.uiFieldBorder.height = FIELD_HEIGHT + 2*FIELD_BORDER;
      this.uiFieldBorder.x = 0;
      this.uiFieldBorder.y = PADDING_BOTTOM;
      this.uiFieldBorder.z = 100;
      this.drawables.push (this.uiFieldBorder);
      
      if (rightSide) {
        this.uiSidebar.x = FIELD_WIDTH;
        this.uiFieldBorder.x = 0;
      } else {
        this.uiSidebar.x = 0;
        this.uiFieldBorder.x = SIDE_WIDTH;
      }
      
      let xTex = FIELD_BLOCKS_WIDE;
      let yTex = FIELD_BLOCKS_HIGH;
      let texCoords = [
        0,    yTex,
        0,    0,
        xTex, 0,
        xTex, yTex,
      ];
      
      let gridBuffer = webgl.gl.createBuffer ();
      webgl.gl.bindBuffer (webgl.gl.ARRAY_BUFFER, gridBuffer);
      webgl.gl.bufferData (webgl.gl.ARRAY_BUFFER, new Float32Array (texCoords), webgl.gl.STATIC_DRAW);
      gridBuffer.itemSize = 2;
      gridBuffer.numItems = texCoords.length;
      
      this.uiField = new SceneGraph.Sprite (gridTexture, gridBuffer, shader);
      this.uiField.parent = this.uiFieldBorder;
      this.uiField.width = FIELD_WIDTH;
      this.uiField.height = FIELD_HEIGHT;
      this.uiField.x = FIELD_BORDER;
      this.uiField.y = FIELD_BORDER;
      this.uiField.z = 0;
      this.drawables.push (this.uiField);
      
      this.uiScoreText = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.uiScoreText.parent = this.uiFieldBorder;
      this.uiScoreText.color = baseColor;
      this.updateScoreText (0);
      this.uiScoreText.scaleX = 2;
      this.uiScoreText.scaleY = 2;
      this.uiScoreText.x = Math.floor (this.uiFieldBorder.width/2 - this.uiScoreText.width);
      this.uiScoreText.y = Math.ceil (-PADDING_BOTTOM/2 - this.uiScoreText.height);
      this.uiScoreText.z = -80;
      this.drawables.push (this.uiScoreText);
      
      // This will hide the blocks when they fall out of the field on game over
      let uiFieldBaseCover = new SceneGraph.Sprite (textureAtlas, 'white', shader);
      uiFieldBaseCover.parent = this.uiFieldBorder;
      uiFieldBaseCover.width = this.uiFieldBorder.width;
      uiFieldBaseCover.height = PADDING_BOTTOM;
      uiFieldBaseCover.x = 0;
      uiFieldBaseCover.y = -uiFieldBaseCover.height;
      uiFieldBaseCover.z = -50;
      this.drawables.push (uiFieldBaseCover);
      
      let uiFieldTopCover = new SceneGraph.Sprite (textureAtlas, 'white', shader);
      uiFieldTopCover.parent = this.uiFieldBorder;
      uiFieldTopCover.width = this.uiFieldBorder.width;
      uiFieldTopCover.height = PADDING_TOP;
      uiFieldTopCover.x = 0;
      uiFieldTopCover.y = this.uiFieldBorder.height;
      uiFieldTopCover.z = -50;
      this.drawables.push (uiFieldTopCover);
      
      this.uiNextContainer = new SceneGraph.Sprite (textureAtlas, 'white', shader);
      this.uiNextContainer.parent = this.uiFieldBorder;
      this.uiNextContainer.color = baseColor;
      this.uiNextContainer.width = NEXT_BOX_WIDTH;
      this.uiNextContainer.height = NEXT_BOX_HEIGHT;
      this.uiNextContainer.x = Math.floor (this.uiFieldBorder.width/2 - this.uiNextContainer.width/2);
      /*if (rightSide) {
        this.uiNextContainer.x = Math.floor (-this.uiNextContainer.width/2);
      } else {
        this.uiNextContainer.x = Math.floor (this.uiFieldBorder.width - this.uiNextContainer.width/2);
      }*/
      this.uiNextContainer.y = Math.floor (this.uiFieldBorder.height + PADDING_TOP/2 - this.uiNextContainer.height/2);
      this.uiNextContainer.z = -80;
      this.drawables.push (this.uiNextContainer);
      
      let uiHoldLabel = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      uiHoldLabel.parent = this.uiSidebar;
      uiHoldLabel.color = baseColor;
      uiHoldLabel.text = 'HOLD';
      uiHoldLabel.scaleX = 2;
      uiHoldLabel.scaleY = 2;
      uiHoldLabel.x = Math.floor (SIDE_WIDTH/2 - uiHoldLabel.width);
      uiHoldLabel.y = this.uiFieldBorder.height - uiHoldLabel.height*2;
      uiHoldLabel.z = 0;
      this.drawables.push (uiHoldLabel);
      
      this.uiHoldContainer = new SceneGraph.Sprite (textureAtlas, 'white', shader);
      this.uiHoldContainer.parent = this.uiSidebar;
      this.uiHoldContainer.color = baseColor;
      this.uiHoldContainer.width = NEXT_BOX_WIDTH;
      this.uiHoldContainer.height = NEXT_BOX_HEIGHT;
      this.uiHoldContainer.x = Math.floor (SIDE_WIDTH/2 - this.uiHoldContainer.width/2);
      this.uiHoldContainer.y = Math.floor (uiHoldLabel.y - this.uiHoldContainer.height - 4);
      this.uiHoldContainer.z = 0;
      this.drawables.push (this.uiHoldContainer);
      
      this.uiLinesText = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.uiLinesText.color = baseColor;
      this.uiLinesText.scaleX = 2;
      this.uiLinesText.scaleY = 2;
      if (twoPlayer) {
        this.uiLinesText.parent = this.uiSidebar;
        this.uiLinesText.y = this.uiFieldBorder.height + 36;
        this.updateLinesText (0);
      } else {
        this.uiLinesText.parent = this;
        this.updateLinesText (0);
        this.uiLinesText.y = PADDING_BOTTOM + this.uiFieldBorder.height - this.uiLinesText.height*2;
      }
      this.uiLinesText.z = 0;
      this.drawables.push (this.uiLinesText);
      
      this.uiLevelText = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.uiLevelText.color = baseColor;
      this.uiLevelText.scaleX = 2;
      this.uiLevelText.scaleY = 2;
      if (twoPlayer) {
        this.uiLevelText.parent = this.uiSidebar;
        this.uiLevelText.y = this.uiFieldBorder.height + 14;
        this.updateLevelText (0);
      } else {
        this.uiLevelText.parent = this;
        this.updateLevelText (0);
        this.uiLevelText.y = PADDING_BOTTOM + this.uiFieldBorder.height - this.uiLinesText.height*2 - this.uiLevelText.height*2 - 16;
      }
      this.uiLevelText.z = 0;
      this.drawables.push (this.uiLevelText);
      
      if (twoPlayer) {
        if (rightSide) {
          this.uiLinesText.x = this.uiLevelText.x = -32;
        } else {
          this.uiLinesText.x = this.uiLevelText.x = Math.floor (SIDE_WIDTH + this.uiFieldBorder.width / 2 - this.uiNextContainer.width / 2 - this.uiLinesText.width*2 - 8);
        }
      } else {
        this.uiLinesText.x = this.uiLevelText.x = SIDE_WIDTH + FIELD_WIDTH + 16;
      }
      
      // These don't move, so we only need to update the matricies once
      this.updateMatricies ();
    }
    
    updateScoreText (value) {
      this.uiScoreText.text = ('0000000' + value).slice (-8);
    }
    
    updateLinesText (value) {
      this.uiLinesText.text = 'LINES ' + ('00' + value).slice (-3);
    }
    
    updateLevelText (value) {
      this.uiLevelText.text = 'LEVEL  ' + ('0' + value).slice (-2);
    }
    
    draw () {
      webgl.drawScene (this.drawables);
    }
  }
  
  class Tetromino extends SceneGraph.Transform {
    constructor (kind, field, buffer) {
      super ();
      
      this.kind = kind;
      this.field = field;
      this.info = TetrominoInfo [kind];
      this.currentRotation = 0;
      
      this.blocks = [];
      for (var i = 0; i < 4; ++i) {
        let block = new SceneGraph.Sprite (
          textureAtlas, 
          buffer, 
          shader);
        
        block.width = BLOCK_DISPLAY_WIDTH;
        block.height = BLOCK_DISPLAY_HEIGHT;
        
        block.parent = this;
        this.blocks.push (block);
      }
      
      this.updateBlockPositions ();
    }
    
    forEachBlock (callback, rotation) {
      if (rotation == undefined) {
        rotation = this.currentRotation;
      }
      var points = this.info.rotations [rotation];
      for (var i = 0; i < points.length; ++i) {
        var point = points [i];
        var col = -1;
        var line = null;
        if (this.col !== undefined) {
          col = this.col + point [0];
        }
        if (this.line !== undefined) {
          line = this.line;
          for (var j = 0; j < Math.abs (point [1]); ++j) {
            line = point [1] > 0 ? line.next : line.prev;
          }
        }
        var block = this.blocks [i];
        if (callback (col, line, block, point) === false) {
          break;
        }
      }
    }
    
    anyBlock (predicate, rotation) {
      var result = false;
      this.forEachBlock ((col, line, block, offset) => {
        if (predicate (col, line, block, offset)) {
          result = true;
          return false;
        }
      }, rotation);
      return result;
    }
    
    updateBlockPositions () {
      this.forEachBlock ((c, r, block, offset) => {
        block.x = +offset [0] * BLOCK_WIDTH;
        block.y = -offset [1] * BLOCK_HEIGHT;
      });
    }
    
    rotate (dir) {
      var rotation = wrap (
        this.currentRotation + dir,
        this.info.rotations.length);
      
      var wallKickDelta = 0;
      
      // Check if the new piece rotation is valid (all blocks end up in an empty cell).
      // If it's not, then see if it can be pushed one cell left or right (wall kick).
      // If neither action leads to a valid move, then don't rotate the piece.
      let blocked = this.anyBlock (this.field.cellOccupied.bind (this.field), rotation);
      if (blocked) {
        this.col += 1;
        wallKickDelta = 1;
        blocked = this.anyBlock (this.field.cellOccupied.bind (this.field), rotation);
        if (blocked) {
          this.col -= 2;
          wallKickDelta = -1;
          blocked = this.anyBlock (this.field.cellOccupied.bind (this.field), rotation);
          if (blocked) {
            this.col += 1;
            wallKickDelta = 0;
          }
        }
      }
      
      if (!blocked) {
        if (wallKickDelta != 0) {
          this.x += BLOCK_WIDTH * wallKickDelta;
        }
        
        this.currentRotation = rotation;
        this.updateBlockPositions ();
        
        return true;
      }
      
      return false;
    }
  }
  
  class TitleScreen extends SceneGraph.Transform {
    constructor () {
      super ();
      
      this.drawables = new Set ();
      
      let title = new SceneGraph.Sprite (textureAtlas, 'title', shader);
      title.parent = this;
      title.x = 32;
      title.y = worldHeight - title.height - 32;
      title.z = 0;
      this.drawables.add (title);
      
      this.selected = TitleScreen.ONE_PLAYER;
      
      this.arrowLeft = new SceneGraph.Sprite (textureAtlas, 'arrow_left', shader);
      this.arrowLeft.parent = this;
      this.arrowLeft.anchorX = 285;
      this.arrowLeft.x = this.arrowLeft.anchorX;
      this.arrowLeft.y = 34;
      this.arrowLeft.z = 0;
      this.drawables.add (this.arrowLeft);
      
      this.arrowRight = new SceneGraph.Sprite (textureAtlas, 'arrow_right', shader);
      this.arrowRight.parent = this;
      this.arrowRight.anchorX = 317;
      this.arrowRight.x = this.arrowRight.anchorX;
      this.arrowRight.y = 34;
      this.arrowRight.z = 0;
      this.drawables.add (this.arrowRight);
      
      let vertices = [
          0,    0,  0,
          0,  120,  0,
        616,  240,  0,
        616,  120,  0,
      ];
      
      let vertexBuffer = webgl.gl.createBuffer ();
      webgl.gl.bindBuffer (webgl.gl.ARRAY_BUFFER, vertexBuffer);
      webgl.gl.bufferData (webgl.gl.ARRAY_BUFFER, new Int16Array (vertices), webgl.gl.STATIC_DRAW);
      vertexBuffer.itemSize = 3;
      vertexBuffer.numItems = vertices.length;
      
      let baseLightColor = [44/255, 60/255, 104/255, 1];
      
      this.solo = new SceneGraph.Sprite (textureAtlas, 'white', shader, vertexBuffer);
      this.solo.parent = this;
      this.solo.color = baseLightColor;
      this.solo.x = -350;
      this.solo.offY = -60;
      this.solo.onY = -20;
      this.solo.targetY = this.solo.onY;
      this.solo.y = this.solo.targetY;
      this.solo.z = 0;
      this.drawables.add (this.solo);
      
      this.soloIcon = new SceneGraph.Sprite (textureAtlas, 'solo_icon_on', shader);
      this.soloIcon.parent = this.solo;
      this.soloIcon.x = 365;
      this.soloIcon.y = 120;
      this.soloIcon.z = 0;
      this.drawables.add (this.soloIcon);
      
      this.soloLabel = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.soloLabel.parent = this.solo;
      this.soloLabel.text = 'SOLO';
      this.soloLabel.scaleX = 4;
      this.soloLabel.scaleY = 4;
      this.soloLabel.x = Math.floor ((this.soloIcon.x + this.soloIcon.width + 616) / 2 - this.soloLabel.width*4 / 2);
      this.soloLabel.y = 165;
      this.soloLabel.z = 0;
      this.drawables.add (this.soloLabel);
      
      this.soloDesc1 = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.soloDesc1.parent = this.solo;
      this.soloDesc1.text = 'HOW LONG CAN';
      this.soloDesc1.x = Math.floor ((this.soloIcon.x + this.soloIcon.width + 616) / 2 - this.soloDesc1.width / 2);
      this.soloDesc1.y = 140;
      this.soloDesc1.z = 0;
      this.drawables.add (this.soloDesc1);
      
      this.soloDesc2 = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.soloDesc2.parent = this.solo;
      this.soloDesc2.text = 'YOU LAST?';
      this.soloDesc2.x = Math.floor ((this.soloIcon.x + this.soloIcon.width + 616) / 2 - this.soloDesc2.width / 2);
      this.soloDesc2.y = 130;
      this.soloDesc2.z = 0;
      this.drawables.add (this.soloDesc2);
      
      this.battle = new SceneGraph.Sprite (textureAtlas, 'white', shader, vertexBuffer);
      this.battle.parent = this;
      this.battle.color = baseLightColor;
      this.battle.x = 290;
      this.battle.offY = 60;
      this.battle.onY = 100;
      this.battle.targetY = this.battle.offY;
      this.battle.y = this.battle.targetY;
      this.battle.z = 0;
      this.drawables.add (this.battle);
      
      this.battleIcon = new SceneGraph.Sprite (textureAtlas, 'battle_icon_on', shader);
      this.battleIcon.parent = this.battle;
      this.battleIcon.x = 231;
      this.battleIcon.y = 70;
      this.battleIcon.z = 0;
      this.drawables.add (this.battleIcon);
      
      this.battleLabel = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.battleLabel.parent = this.battle;
      this.battleLabel.text = 'BATTLE';
      this.battleLabel.scaleX = 4;
      this.battleLabel.scaleY = 4;
      this.battleLabel.x = Math.floor (this.battleIcon.x / 2 - this.battleLabel.width*4 / 2);
      this.battleLabel.y = 80;
      this.battleLabel.z = 0;
      this.drawables.add (this.battleLabel);
      
      this.battleDesc1 = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.battleDesc1.parent = this.battle;
      this.battleDesc1.text = 'FORCE YOUR OPPONENT';
      this.battleDesc1.x = Math.floor (this.battleIcon.x / 2 - this.battleDesc1.width / 2);
      this.battleDesc1.y = 55;
      this.battleDesc1.z = 0;
      this.drawables.add (this.battleDesc1);
      
      this.battleDesc2 = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      this.battleDesc2.parent = this.battle;
      this.battleDesc2.text = 'TO TOP OUT';
      this.battleDesc2.x = Math.floor (this.battleIcon.x / 2 - this.battleDesc2.width / 2);
      this.battleDesc2.y = 45;
      this.battleDesc2.z = 0;
      this.drawables.add (this.battleDesc2);
      
      this.onselect = null;
      
      this.updateSelectionDisplay ();
    }
    
    update () {
      var delta = 0;
      if (game.keyPressedThisFrame [KEYS_RIGHT [0]]) {
        delta = 1;
      } else if (game.keyPressedThisFrame [KEYS_LEFT [0]]) {
        delta = -1;
      }
      if (delta != 0) {
        this.selected = wrap (this.selected + 1, 2);
        this.updateSelectionDisplay ();
        
        audio.sfx.move_piece.currentTime = 0;
        audio.sfx.move_piece.play ();
      }
      
      if (this.solo.y != this.solo.targetY) {
        let sign = Math.sign (this.solo.targetY - this.solo.y);
        this.solo.y += sign*15;
        if (Math.sign (this.solo.targetY - this.solo.y) != sign) {
          this.solo.y = this.solo.targetY;
        }
      }
      if (this.battle.y != this.battle.targetY) {
        let sign = Math.sign (this.battle.targetY - this.battle.y);
        this.battle.y += sign*15;
        if (Math.sign (this.battle.targetY - this.battle.y) != sign) {
          this.battle.y = this.battle.targetY;
        }
      }
      
      this.arrowLeft.x = this.arrowLeft.anchorX - Math.floor ((Math.sin (game.time * 10) + 1) / 2 * 3);
      this.arrowRight.x = this.arrowRight.anchorX + Math.floor ((Math.sin (game.time * 10) + 1) / 2 * 3);
      
      if (game.keyPressedThisFrame [KEY_START]) {
        if (this.onselect) {
          this.onselect (this.selected);
        }
      }
    }
    
    updateSelectionDisplay () {
      let offTextColor = [85/255, 102/255, 150/255, 1];
      let whiteColor = [1, 1, 1, 1];
      
      if (this.selected == 0) {
        this.solo.targetY = this.solo.onY;
        this.battle.targetY = this.battle.offY;
        
        this.soloLabel.color = whiteColor;
        this.soloDesc1.color = whiteColor;
        this.soloDesc2.color = whiteColor;
        this.battleLabel.color = offTextColor;
        this.battleDesc1.color = offTextColor;
        this.battleDesc2.color = offTextColor;
        
        this.soloIcon.sprite = 'solo_icon_on';
        this.battleIcon.sprite = 'battle_icon_off';
      } else {
        this.solo.targetY = this.solo.offY;
        this.battle.targetY = this.battle.onY;
        
        this.soloLabel.color = offTextColor;
        this.soloDesc1.color = offTextColor;
        this.soloDesc2.color = offTextColor;
        this.battleLabel.color = whiteColor;
        this.battleDesc1.color = whiteColor;
        this.battleDesc2.color = whiteColor;
        
        this.soloIcon.sprite = 'solo_icon_off';
        this.battleIcon.sprite = 'battle_icon_on';
      }
    }
    
    draw () {
      this.updateMatricies ();
      webgl.drawScene (this.drawables);
    }
  }
  TitleScreen.ONE_PLAYER = 0;
  TitleScreen.TWO_PLAYER = 1;
  
  function createPauseScreen () {
    let pauseText = new FixedWidthText (textureAtlas, shader, fontInfo);
    pauseText.text = 'PAUSE';
    pauseText.color = [0.573, 0.565, 1, 1];
    pauseText.x = Math.floor (worldWidth/2 - pauseText.width/2);
    pauseText.y = Math.ceil (worldHeight/2 - pauseText.height/2);
    pauseScene.add (pauseText);
  }
  
  function scoreForLinesCleared (level, lines) {
    let baseScore = [0, 40, 100, 300, 1200];
    return baseScore [lines] * (level+1);
  }
  
  function tSpinBonusForLines (level, lines) {
    let baseScore = [600, 1200, 1800, 2400, 0 /* N/A */];
    return baseScore [lines] * (level+1);
  }
  
  function scoreForPerfectClear (level) {
    return 1200 * (level+1);
  }
  
  function levelForLines (lines) {
    return Math.floor (lines / 10);
  }
  
  function dropDelayForLevel (level) {
    let lowLevelDropDelay = [
      0.799, 0.715, 0.632, 0.549, 0.466, 0.383, 0.300, 0.216, 0.133, 0.100];
    if (level <= 9) {
      return lowLevelDropDelay [level];
    } else if (level >= 10 && level <= 12) {
      return 0.083;
    } else if (level >= 13 && level <= 15) {
      return 0.067;
    } else if (level >= 16 && level <= 18) {
      return 0.050;
    } else if (level >= 19 && level <= 28) {
      return 0.033;
    } else {
      return 0.017;
    }
  }
  
  function randomElement (array) {
    return array [Math.floor (Math.random () * array.length)];
  }
  
  function wrap (i, n) {
    i %= n;
    return (i < 0 ? i + n : i);
  }
  
  let TetrominoInfo = {
    'I': {
      center: {
        x: 0,
        y: 0.5,
      },
      rotations: [
        [[-2,  0], [-1,  0], [0, 0], [1, 0]],
        [[ 0, -2], [ 0, -1], [0, 0], [0, 1]],
      ],
      blockType: 1,
    },
    'O': {
      center: {
        x: 0,
        y: 0,
      },
      rotations: [
        [[-1, 0], [0, 0], [-1, 1], [0, 1]],
      ],
      blockType: 1,
    },
    'T': {
      center: {
        x: 0.5,
        y: 0,
      },
      rotations: [
        [[-1, 0], [0, 0], [1, 0], [0, 1]],
        [[0, -1], [0, 0], [0, 1], [-1, 0]],
        [[-1, 1], [0, 1], [1, 1], [0, 0]],
        [[0, -1], [0, 0], [0, 1], [1, 0]],
      ],
      tspinOriginShift: [
        false,
        false,
        true,
        false,
      ],
      blockType: 1,
    },
    'J': {
      center: {
        x: 0.5,
        y: 0,
      },
      rotations: [
        [[-1, 0], [0, 0], [1, 0], [1, 1]],
        [[0, -1], [0, 0], [0, 1], [-1, 1]],
        [[-1, 1], [0, 1], [1, 1], [-1, 0]],
        [[0, -1], [0, 0], [0, 1], [1, -1]],
      ],
      blockType: 3,
    },
    'L': {
      center: {
        x: 0.5,
        y: 0,
      },
      rotations: [
        [[-1, 0], [0, 0], [1, 0], [-1, 1]],
        [[0, -1], [0, 0], [0, 1], [-1, -1]],
        [[-1, 1], [0, 1], [1, 1], [1, 0]],
        [[0, -1], [0, 0], [0, 1], [1, 1]],
      ],
      blockType: 2,
    },
    'S': {
      center: {
        x: 0.5,
        y: 0,
      },
      rotations: [
        [[0, 0], [1, 0], [-1, 1], [0, 1]],
        [[0, 0], [0, 1], [-1, -1], [-1, 0]],
      ],
      blockType: 3,
    },
    'Z': {
      center: {
        x: 0.5,
        y: 0,
      },
      rotations: [
        [[-1, 0], [0, 0], [0, 1], [1, 1]],
        [[0, -1], [0, 0], [-1, 0], [-1, 1]],
      ],
      blockType: 2,
    },
  };
  
  let STATE_TITLE_SCREEN = 0;
  let STATE_IN_GAME = 1;
  
  
  
  var state;
  var gameOver = false;
  var paused = false;
  var tetrisFields = [];
  
  
  
  
  function update () {
    if (!gameOver) {
      if (paused) {
        if (game.keyPressedThisFrame [KEY_START]) {
          paused = false;
          audio.music.tetris.play ();
        }
        return;
      } else {
        if (game.keyPressedThisFrame [KEY_START]) {
          paused = true;
          audio.music.tetris.pause ();
          audio.sfx.pause.play ();
          webgl.drawScene (pauseScene);
          return;
        }
      }
    }
    
    for (let field of tetrisFields) {
      field.update ();
    }
  }
  
  function onLineClear (field, numLines) {
    for (let other of tetrisFields) {
      if (field != other) {
        other.addGarbageLines (numLines == 4 ? 4 : numLines - 1);
      }
    }
  }
  
  function onGameOver (field) {
    gameOver = true;
    
    audio.music.tetris.pause ();
    
    audio.sfx.game_over.currentTime = 0;
    audio.sfx.game_over.play ();
    
    for (let other of tetrisFields) {
      if (field != other) {
        other.onWin ();
      }
    }
  }
  
  function startGame (mode) {
    webgl.bgcolor = [1, 1, 1, 1];
    
    let randomSeed = new Date ().getTime ();
    
    if (mode == TitleScreen.TWO_PLAYER) {
      uiVS = new SceneGraph.FixedWidthText (textureAtlas, fontInfo, shader);
      uiVS.text = 'VS';
      uiVS.color = baseColor;
      uiVS.scaleX = 2;
      uiVS.scaleY = 2;
      uiVS.x = Math.floor (worldWidth/2 - uiVS.width);
      uiVS.y = Math.floor (PADDING_BOTTOM + (FIELD_HEIGHT + FIELD_BORDER*2)/2 - uiVS.height*2/2);
      uiVS.z = 0;
      
      tetrisFields.push (new TetrisGame (
        FIELD_BLOCKS_WIDE,
        FIELD_BLOCKS_HIGH,
        BLOCK_WIDTH,
        BLOCK_HEIGHT,
        randomSeed,
        true,
        false,
        {
          left: KEYS_LEFT [0],
          right: KEYS_RIGHT [0],
          softDrop: KEYS_SOFT_DROP [0],
          hardDrop: KEYS_HARD_DROP [0],
          hold: KEYS_HOLD [0],
          rotateCCW: KEYS_ROTATE_CCW [0],
          rotateCW: KEYS_ROTATE_CW [0],
        }));
      
      tetrisFields.push (new TetrisGame (
        FIELD_BLOCKS_WIDE,
        FIELD_BLOCKS_HIGH,
        BLOCK_WIDTH,
        BLOCK_HEIGHT,
        randomSeed,
        true,
        true,
        {
          left: KEYS_LEFT [1],
          right: KEYS_RIGHT [1],
          softDrop: KEYS_SOFT_DROP [1],
          hardDrop: KEYS_HARD_DROP [1],
          hold: KEYS_HOLD [1],
          rotateCCW: KEYS_ROTATE_CCW [1],
          rotateCW: KEYS_ROTATE_CW [1],
        }));
    } else {
      tetrisFields.push (new TetrisGame (
        FIELD_BLOCKS_WIDE,
        FIELD_BLOCKS_HIGH,
        BLOCK_WIDTH,
        BLOCK_HEIGHT,
        randomSeed,
        false,
        false,
        {
          left: KEYS_LEFT [0],
          right: KEYS_RIGHT [0],
          softDrop: KEYS_SOFT_DROP [0],
          hardDrop: KEYS_HARD_DROP [0],
          hold: KEYS_HOLD [0],
          rotateCCW: KEYS_ROTATE_CCW [0],
          rotateCW: KEYS_ROTATE_CW [0],
        }));
    }
    
    for (let field of tetrisFields) {
      field.onlineclear = onLineClear;
      field.ongameover = onGameOver;
      field.startGame ();
    }
    
    state = STATE_IN_GAME;
    audio.music.tetris.play ();
  }
  
  var uiVS;
  function createScenesAndStartGameLoop () {
    state = STATE_TITLE_SCREEN;
    
    createPauseScreen ();
    
    webgl.bgcolor = baseColor;
    
    let titleScreen = new TitleScreen ();
    titleScreen.onselect = startGame;
    
    game.start (function () {
      if (state == STATE_TITLE_SCREEN) {
        titleScreen.update ();
        webgl.clear ();
        titleScreen.draw ();
      }
      else if (state == STATE_IN_GAME) {
        update ();
        if (! paused) {
          webgl.clear ();
          if (uiVS) {
            webgl.drawSprite (uiVS);
          }
          for (let field of tetrisFields) {
            field.draw ();
          }
        }
      }
    });
  }
  
  webgl.loadResources (createScenesAndStartGameLoop);
})();
