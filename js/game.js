// public
var game = {};


// private
(function () {
  game.targetFPS = 60;
  
  game.time = 0;
  game.deltaTime = 0;
  var previousTime = 0;
  var isFirstFrame = true;
  
  
  game.keyDown = {};
  game.keyPressedThisFrame = {};
  
  
  var onFrameUpdate;
  var _requestAnimationFrame = window.requestAnimationFrame ||
                               window.webkitRequestAnimationFrame ||
                               window.mozRequestAnimationFrame;
  
  if (_requestAnimationFrame) {
    onFrameUpdate = function (cb) {
      var _cb = function () {
        cb ();
        _requestAnimationFrame (_cb);
      }
      _cb ();
    };
  } else {
    onFrameUpdate = function (cb) {
      setInterval (cb, 1000 / game.targetFPS);
    }
  }
  
  
  document.onkeydown = function (e) {
    game.keyDown [e.keyCode] = true;
  }
  
  document.onkeyup = function (e) {
    delete game.keyDown [e.keyCode];
    delete game.keyPressedThisFrame [e.keyCode];
  }
  
  window.addEventListener ('blur', function (e) {
    game.keyDown = {};
    game.keyPressedThisFrame = {};
  });
  
  
  game.start = 
  function (updateFunc) {
    onFrameUpdate (function () {
      game.time = Date.now () / 1000;
      if (isFirstFrame) {
        isFirstFrame = false;
        game.deltaTime = 1 / game.targetFPS;
      } else {
        game.deltaTime = game.time - previousTime;
      }
      
      // For all keys that are down, set that they were pressed this frame if
      // the keyPressedThisFrame object doesn't have that key set.
      // These keys will be set to false after the first frame until the key is
      // released, after which they will be deleted.
      for (var key of Object.keys (game.keyDown)) {
        if (! game.keyPressedThisFrame.hasOwnProperty (key)) {
          game.keyPressedThisFrame [key] = true;
        }
      }
      
      if (game.deltaTime >= 1 / game.targetFPS) {
        previousTime = game.time;
        
        updateFunc ();
        
        for (var key of Object.keys (game.keyPressedThisFrame)) {
          game.keyPressedThisFrame [key] = false;
        }
      }
    });
  };
})();
